# gd103+gnu+rt-thread

#### 介绍
采用GCC编译GD32F103，makefile组织管理工程，同时移植了rt-thread的模板。

#### 软件架构
软件架构说明

    ├── Drivers
    │   ├── CMSIS
    │   │   ├── GD
    │   │   │   └── GD32F1xx
    │   │   │       ├── Include
    │   │   │       │   ├── gd32f10x.h        
    │   │   │       │   ├── gd32f10x_conf.h   
    │   │   │       │   └── system_gd32f10x.h 
    │   │   │       └── Source
    │   │   │           └── GCC
    │   │   │               ├── isr_tab.s     
    │   │   │               └── startup_gd32.s
    │   │   ├── cmsis_armcc.h
    │   │   ├── cmsis_armclang.h
    │   │   ├── cmsis_compiler.h
    │   │   ├── cmsis_gcc.h
    │   │   ├── cmsis_iccarm.h
    │   │   ├── cmsis_version.h
    │   │   ├── core_armv8mbl.h
    │   │   ├── core_armv8mml.h
    │   │   ├── core_cm0.h
    │   │   ├── core_cm0plus.h
    │   │   ├── core_cm1.h
    │   │   ├── core_cm23.h
    │   │   ├── core_cm3.c
    │   │   ├── core_cm3.h
    │   │   ├── core_cm33.h
    │   │   ├── core_cm4.h
    │   │   ├── core_cm7.h
    │   │   ├── core_cmFunc.h
    │   │   ├── core_cmInstr.h
    │   │   ├── core_sc000.h
    │   │   ├── core_sc300.h
    │   │   ├── mpu_armv7.h
    │   │   ├── mpu_armv8.h
    │   │   └── tz_context.h
    │   └── GD32F1xx_standard_peripheral      
    │       ├── Include
    │       │   ├── RTE_Components.h
    │       │   ├── gd32f10x_adc.h
    │       │   ├── gd32f10x_bkp.h
    │       │   ├── gd32f10x_can.h
    │       │   ├── gd32f10x_conf.h
    │       │   ├── gd32f10x_crc.h
    │       │   ├── gd32f10x_dac.h
    │       │   ├── gd32f10x_dbg.h
    │       │   ├── gd32f10x_dma.h
    │       │   ├── gd32f10x_enet.h
    │       │   ├── gd32f10x_exmc.h
    │       │   ├── gd32f10x_exti.h
    │       │   ├── gd32f10x_fmc.h
    │       │   ├── gd32f10x_fwdgt.h
    │       │   ├── gd32f10x_gpio.h
    │       │   ├── gd32f10x_i2c.h
    │       │   ├── gd32f10x_libopt.h
    │       │   ├── gd32f10x_misc.h
    │       │   ├── gd32f10x_pmu.h
    │       │   ├── gd32f10x_rcu.h
    │       │   ├── gd32f10x_rtc.h
    │       │   ├── gd32f10x_sdio.h
    │       │   ├── gd32f10x_spi.h
    │       │   ├── gd32f10x_timer.h
    │       │   ├── gd32f10x_usart.h
    │       │   ├── gd32f10x_wwdgt.h
    │       │   └── system_gd32f10x.h
    │       └── Source
    │           ├── gd32f10x_adc.c
    │           ├── gd32f10x_bkp.c
    │           ├── gd32f10x_can.c
    │           ├── gd32f10x_crc.c
    │           ├── gd32f10x_dac.c
    │           ├── gd32f10x_dbg.c
    │           ├── gd32f10x_dma.c
    │           ├── gd32f10x_enet.c
    │           ├── gd32f10x_exmc.c
    │           ├── gd32f10x_exti.c
    │           ├── gd32f10x_fmc.c
    │           ├── gd32f10x_fwdgt.c
    │           ├── gd32f10x_gpio.c
    │           ├── gd32f10x_i2c.c
    │           ├── gd32f10x_misc.c
    │           ├── gd32f10x_pmu.c
    │           ├── gd32f10x_rcu.c
    │           ├── gd32f10x_rtc.c
    │           ├── gd32f10x_sdio.c
    │           ├── gd32f10x_spi.c
    │           ├── gd32f10x_timer.c
    │           ├── gd32f10x_usart.c
    │           ├── gd32f10x_wwdgt.c
    │           └── system_gd32f1xx.c
    ├── Makefile
    ├── README.md
    ├── RT-Thread
    │   ├── bsp
    │   │   ├── board.c
    │   │   └── rtconfig.h
    │   ├── components
    │   │   └── finsh
    │   │       ├── cmd.c
    │   │       ├── finsh.h
    │   │       ├── finsh_api.h
    │   │       ├── msh.c
    │   │       ├── msh.h
    │   │       ├── shell.c
    │   │       └── shell.h
    │   ├── include
    │   │   ├── libc
    │   │   │   ├── libc_dirent.h
    │   │   │   ├── libc_errno.h
    │   │   │   ├── libc_fcntl.h
    │   │   │   ├── libc_fdset.h
    │   │   │   ├── libc_ioctl.h
    │   │   │   ├── libc_signal.h
    │   │   │   └── libc_stat.h
    │   │   ├── rtdbg.h
    │   │   ├── rtdebug.h
    │   │   ├── rtdef.h
    │   │   ├── rthw.h
    │   │   ├── rtlibc.h
    │   │   ├── rtm.h
    │   │   ├── rtservice.h
    │   │   └── rtthread.h
    │   ├── libcpu
    │   │   └── arm
    │   │       └── cortex-m3
    │   │           ├── context_gcc.s
    │   │           └── cpuport.c
    │   └── src
    │       ├── clock.c
    │       ├── components.c
    │       ├── cpu.c
    │       ├── device.c
    │       ├── idle.c
    │       ├── ipc.c
    │       ├── irq.c
    │       ├── kservice.c
    │       ├── mem.c
    │       ├── memheap.c
    │       ├── mempool.c
    │       ├── object.c
    │       ├── scheduler.c
    │       ├── slab.c
    │       ├── thread.c
    │       └── timer.c
    ├── app
    │   └── main.c
    ├── burn
    │   ├── download.jlink
    │   └── jlinkload.bat
    ├── burn.bat
    ├── gd32F103RBTX_FLASH.ld
    └── startup_gd32f103xb.s

#### 使用说明

1.  切换到当前目录，执行$ make -j8，会生成build文件夹，编译的文件在build文件夹下面
2.  修改burn目录下的脚本命令，执行 burn.bat可在windows下调用jlink烧录
3.  也可以直接用jflash烧录build下的bin文件






